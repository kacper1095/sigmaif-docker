FROM tensorflow/tensorflow:1.9.0-gpu-py3

ENV CMAKE_VERSION 3.12
ENV CMAKE_BUILD 3
ENV PYTHONPATH=/root/miniconda3/bin
ENV PATH=/root/miniconda3/bin:$PATH

# GENERAL
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        gnupg-agent \
        software-properties-common \
        git \
        curl \
        gcc-5 g++-5 \
        vim \
        wget \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 100 \
    && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 100

# SigmaIf-Building
RUN mkdir /temp \
    && cd /temp \
    && wget https://cmake.org/files/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.${CMAKE_BUILD}.tar.gz \
    && tar -xzvf cmake-${CMAKE_VERSION}.${CMAKE_BUILD}.tar.gz \
    && cd cmake-${CMAKE_VERSION}.${CMAKE_BUILD} \
    && ./bootstrap \
    && make -j`nproc` \
    && make install \
    && cp bin/cmake /usr/bin/cmake

RUN cd /usr/local/cuda/lib64 \
    && mv stubs/libcuda.so ./ \
    && ln -s libcuda.so libcuda.so.1 \
    && ldconfig

WORKDIR /root/

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && chmod +x Miniconda3-latest-Linux-x86_64.sh \
    && ./Miniconda3-latest-Linux-x86_64.sh -b \
    && conda init bash

RUN conda install -y \
    python=3.6 \
    Pillow \
    h5py \
    ipykernel \
    jupyter \
    matplotlib \
    numpy \
    pandas \
    scipy \
    scikit-learn \
    && python -m ipykernel.kernelspec

RUN conda install -c anaconda -y tensorflow-gpu=1.9.0

WORKDIR /projects

# SigmaIf-Building 
COPY sigmaif-building /projects/sigmaif-building
RUN mkdir /projects/sigmaif-building/projects/sigmaif/build \
    && cd /projects/sigmaif-building/projects/sigmaif/build \
    && cmake .. \
    && make -j`nproc` \
    && cd /projects


# SigmaIf-Tensorflow 
COPY sigmaif-tensorflow /projects/sigmaif-tensorflow
RUN cd sigmaif-tensorflow \
    && conda env create -f env.yaml \
    && ./update_library.sh

ENV PYTHONPATH=/root/miniconda3/bin/envs/sigmaif/bin/
ENV PATH=/root/miniconda3/envs/sigmaif/bin/:$PATH

RUN cd sigmaif-tensorflow \
    && python setup.py install \
    && python -c "from sigmaif import layer; layer.SigmaIf" \
    && python -m pytest tests -p no:warnings -s \
    && visible_devices=$CUDA_VISIBLE_DEVICES \
    && CUDA_VISIBLE_DEVICES= python -m pytest tests -p no:warnings -s \
    && CUDA_VISIBLE_DEVICES=$visible_devices \
    && cd .. \
    && conda clean --all -y

# Sigmaif-Testing
COPY sigmaif-testing /projects/sigmaif-testing
RUN cd sigmaif-testing \
    && mkdir experiments

RUN conda init --all && echo "conda activate sigmaif" >> /root/.bashrc
WORKDIR /projects/sigmaif-testing

COPY run-jupyter.sh /projects/

CMD /projects/run-jupyter.sh
