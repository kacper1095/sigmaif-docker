#!/bin/bash

if ! [ -x "$(command -v git-lfs)" ]; then
  curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
      && apt-get update \
      && apt-get install -y --no-install-recommends git-lfs \
      && git lfs install
fi

if ! [ -x "$(command -v nvidia-docker)" ]; then
  os=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
  if [ "$os" = "Ubuntu" ]; then
    bash nvidia-docker/ubuntu-install-nvidia-docker.sh
  elif [ "$of" = "CentOS" ]; then
    bash nvidia-docker/centos-install-nvidia-docker.sh
  else
    bash nvidia-docker/ubuntu-install-nvidia-docker.sh
  fi
fi

if ! [ -e "sigmaif-building" ]; then
  git clone https://gitlab.com/kacper1095/sigmaif-building.git sigmaif-building
fi

if ! [ -e "sigmaif-tensorflow" ]; then
  git clone https://gitlab.com/kacper1095/sigmaif-tensorflow.git sigmaif-tensorflow
fi

if ! [ -e "sigmaif-testing" ]; then
  git clone https://gitlab.com/kacper1095/sigmaif-testing.git sigmaif-testing
fi 

docker-compose build
docker-compose up -d && docker-compose exec app bash