# Sigmaif Docker

## Description

Purpose of this repository is automate the whole process of the sigma-if experiments installation. This includes downloading the following repositories with their purpose:
- [sigmaif-building](https://gitlab.com/kacper1095/sigmaif-building) - contains the implementation of SigmaIf and FFA layers in pure C++ and CUDA for Tensorflow. The repo allows to build a dynamic library that can be further included in a python code.
- [sigmaif-tensorflow](https://gitlab.com/kacper1095/sigmaif-tensorflow) - contains a code used to implement SigmaIf and FFA as layers in Tensorflow as pythonic code. Thus, the usage is almost transparent in comparison to standard layers, such as `dense`
- [sigmaif-testing](https://gitlab.com/kacper1095/sigmaif-testing) - contains a code used during MSc Thesis to perform the experiments and analyse the results.

## Requirements
- Ubuntu or CentOS system
- nvidia driver, version >= 396 and < 418

## Installation
1. Clone this repository (or copy it from the disc to local directory if the disc is provided) and enter to directory containing it
2. If you are a root, then run `./setup.sh`, and `sudo ./setup.sh` otherwise. This will install `git-lfs` and `nvidia-docker2` if necessary, build docker and enter it afterwards. The installation process can take up to ~2h.

Now, you can do the following things:
1. Run experiments with: `python experiments.py` inside `/projects/sigmaif-testing` directory ...
2. Or run `jupyter notebook` by entering `localhost:8123/lab` in your browser. Then, you'll be able to browse results obtained by running `python experiments.py`. Run all cells to get plots and tables.