#!/bin/bash

jupyter lab --ip='0.0.0.0' --NotebookApp.token='' --NotebookApp.password='' --port='8048' --notebook-dir='/projects/sigmaif-testing' --allow-root --no-browser